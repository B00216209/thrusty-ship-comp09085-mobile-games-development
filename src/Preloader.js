/**
 * Created with Webstorm using the template from https://github.com/photonstorm/phaser/tree/master/resources/Project%20Templates/Full%20Screen%20Mobile
 * User: B00216209
 */
BasicGame.Preloader = function (game) {
	this.background;    // background image
	this.preloadBar;    // preload back forground image
    this.preloadBarBackground;  // preload back backgroud image
};

BasicGame.Preloader.prototype = {

	preload: function(){
        // Setup background image
        this.background = this.game.add.sprite(0, 0,'spacescape');

        // Setup preload bar
        this.preloadBarBackground = this.add.sprite(this.world.centerX, this.world.centerY, 'preloaderBarBackground');
        this.preloadBarBackground.anchor.setTo(0.5,0.5);
        this.preloadBar = this.add.sprite(this.world.centerX, this.world.centerY, 'preloaderBarForeground');
        this.preloadBar.anchor.setTo(0.5,0.5);

		//	This sets the preloadBar sprite as a loader sprite.
		//	What that does is automatically crop the sprite from 0 to full-width
		//	as the files below are loaded in.
		this.load.setPreloadSprite(this.preloadBar);

		//	Here we load the rest of the assets our game needs. With the audio loaded first to give it time to decode.
        this.load.audio('intro', 'audio/intro.mp3');
        this.load.audio('pickup', 'audio/pickup.mp3');
        this.load.audio('level1', 'audio/level1.mp3');
        this.load.audio('explosion', 'audio/explosion.mp3');
        this.load.image('SplashLogo', 'images/Splash.png');
        this.load.image('destroyer', 'images/spaceship.png');
        this.load.image('planet3', 'images/p3shaded.png');
        this.load.image('planet4', 'images/p4shaded.png');
        this.load.image('planet7', 'images/p7shaded.png');
        this.load.image('Asteroids', 'images/Asteroids.png');
        this.load.image('moon', 'images/moon.png');
        this.load.image('LivesText', 'images/Lives-Text.png');
        this.load.image('HighScoreBackground','images/HighScoreBackground.png');
        this.load.spritesheet('NewGameButton', 'images/NewGame-Button.png',220, 58);
        this.load.spritesheet('LevelSelectButton', 'images/LevelSelect-Button.png',220, 58);
        this.load.spritesheet('SubmitButton', 'images/Submit-Button.png',220, 58);
        this.load.spritesheet('HighScoreButton', 'images/HighScore-Button.png', 220, 58);
        this.load.spritesheet('MainMenuButton', 'images/MainMenu-Button.png', 220, 58);
        this.load.spritesheet('RestartButton', 'images/Restart-Button.png', 220, 58);
        this.load.spritesheet('alienArtifact', 'images/alien-spritesheet_32-32.png',32, 32, 15);
        this.load.spritesheet('explosion', 'images/explosion_spritesheetL.png', 60, 60);
        this.load.spritesheet('Level1Button', 'images/Level1-Button.png', 163, 163);
        this.load.spritesheet('Level2Button', 'images/Level2-Button.png', 163, 163);
        this.load.spritesheet('Level3Button', 'images/Level3-Button.png', 163, 163);
        this.load.spritesheet('Level4Button', 'images/Level4-Button.png', 163, 163);
        this.load.spritesheet('Level5Button', 'images/Level5-Button.png', 163, 163);
        this.load.spritesheet('Level6Button', 'images/Level6-Button.png', 163, 163);
        this.load.spritesheet('Level7Button', 'images/Level7-Button.png', 163, 163);
        this.load.spritesheet('Level8Button', 'images/Level8-Button.png', 163, 163);
        this.load.spritesheet('Level9Button', 'images/Level9-Button.png', 163, 163);
        this.load.spritesheet('Level10Button', 'images/Level10-Button.png', 163, 163);
        this.load.spritesheet('Level11Button', 'images/Level11-Button.png', 163, 163);
        this.load.spritesheet('Level12Button', 'images/Level12-Button.png', 163, 163);
        this.load.bitmapFont('HighLightFont','images/ThrustyShip.png','images/ThrustyShip.fnt');
    },

	create: function () {
 		//	Once the load has finished we disable the crop because we're going to sit in the update loop for a short while as the music decodes
		this.preloadBar.cropEnabled = false;
        this.ready = false;
	},

	update: function () {
		//	You don't actually need to do this, but I find it gives a much smoother game experience.
		//	Basically it will wait for our audio file to be decoded before proceeding to the MainMenu.
		//	You can jump right into the menu if you want and still play the music, but you'll have a few
		//	seconds of delay while the mp3 decodes - so if you need your music to be in-sync with your menu
		//	it's best to wait for it to decode here first, then carry on.
		
		//	If you don't have any music in your game then put the game.state.start line into the create function and delete
		//	the update function completely.
		
		if (this.cache.isSoundDecoded('intro') && this.ready == false)
		{
			this.ready = true;
			this.state.start('MainMenu');
		}
	}
};
