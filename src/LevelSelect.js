/**
 * Created with Webstorm using the template from https://github.com/photonstorm/phaser/tree/master/resources/Project%20Templates/Full%20Screen%20Mobile
 * User: B00216209
 */
BasicGame.LevelSelect = function (game){
    this.music; // music object used to play intro music
    this.level1Button;  // level 1 button object
    this.level2Button;  // level 2 button object
    this.level3Button;  // level 3 button object
    this.level4Button;  // level 4 button object
    this.level5Button;  // level 5 button object
    this.level6Button;  // level 6 button object
    this.level7Button;  // level 7 button object
    this.level8Button;  // level 8 button object
    this.level9Button;  // level 9 button object
    this.level10Button; // level 10 button object
    this.level11Button; // level 11 button object
    this.level12Button; // level 12 button object
    this.returnToMenuButton;    // return to menu button
    this.goToHighScoreButton; // go to highscore state button
};

BasicGame.LevelSelect.prototype = {

    create: function(){
        // Setup background image
        this.game.add.sprite(0, 0,'spacescape');

        // Setup intro music and start it playing
        this.music = this.add.audio('intro');
        this.music.play();

        // Setup Level Select buttons, checking if the user has previous beat the level or not when chosing the frames.
        if (this.game.levelManager.arrayOfHighScores[0] != this.game.levelManager.arrayOfMaxScores[0])
            this.level1Button = this.add.button(50, 65, 'Level1Button', this.goToLevel1, this,3,2,3,2);
        else
            this.level1Button = this.add.button(50, 65, 'Level1Button', this.goToLevel1, this,1,0,1,0);
        this.level1Button.anchor.setTo(0.0,0.0);

        if (this.game.levelManager.arrayOfHighScores[1] != this.game.levelManager.arrayOfMaxScores[1])
            this.level2Button = this.add.button(307, 65, 'Level2Button', this.goToLevel2, this,3,2,3,2);
        else
            this.level2Button = this.add.button(307, 65, 'Level2Button', this.goToLevel2, this,1,0,1,0);
        this.level2Button.anchor.setTo(0.0,0.0);

        if (this.game.levelManager.arrayOfHighScores[2] != this.game.levelManager.arrayOfMaxScores[2])
            this.level3Button = this.add.button(562, 65, 'Level3Button', this.goToLevel3, this,3,2,3,2);
        else
            this.level3Button = this.add.button(562, 65, 'Level3Button', this.goToLevel3, this,1,0,1,0);
        this.level3Button.anchor.setTo(0.0,0.0);

        if (this.game.levelManager.arrayOfHighScores[3] != this.game.levelManager.arrayOfMaxScores[3])
            this.level4Button = this.add.button(818, 65, 'Level4Button', this.goToLevel4, this,3,2,3,2);
        else
            this.level4Button = this.add.button(818, 65, 'Level4Button', this.goToLevel4, this,1,0,1,0);
        this.level4Button.anchor.setTo(0.0,0.0);

        if (this.game.levelManager.arrayOfHighScores[4] != this.game.levelManager.arrayOfMaxScores[4])
            this.level5Button = this.add.button(50, 258, 'Level5Button', this.goToLevel5, this,3,2,3,2);
        else
            this.level5Button = this.add.button(50, 258, 'Level5Button', this.goToLevel5, this,1,0,1,0);
        this.level5Button.anchor.setTo(0.0,0.0);

        if (this.game.levelManager.arrayOfHighScores[5] != this.game.levelManager.arrayOfMaxScores[5])
            this.level6Button = this.add.button(307, 258, 'Level6Button', this.goToLevel6, this,3,2,3,2);
        else
            this.level6Button = this.add.button(307, 258, 'Level6Button', this.goToLevel6, this,1,0,1,0);
        this.level6Button.anchor.setTo(0.0,0.0);

        if (this.game.levelManager.arrayOfHighScores[6] != this.game.levelManager.arrayOfMaxScores[6])
            this.level7Button = this.add.button(562, 258, 'Level7Button', this.goToLevel7, this,3,2,3,2);
        else
            this.level7Button = this.add.button(562, 258, 'Level7Button', this.goToLevel7, this,1,0,1,0);
        this.level7Button.anchor.setTo(0.0,0.0);

        if (this.game.levelManager.arrayOfHighScores[7] != this.game.levelManager.arrayOfMaxScores[7])
            this.level8Button = this.add.button(818, 258, 'Level8Button', this.goToLevel8, this,3,2,3,2);
        else
            this.level8Button = this.add.button(818, 258, 'Level8Button', this.goToLevel8, this,1,0,1,0);
        this.level8Button.anchor.setTo(0.0,0.0);

        if (this.game.levelManager.arrayOfHighScores[8] != this.game.levelManager.arrayOfMaxScores[8])
            this.level9Button = this.add.button(50, 448, 'Level9Button', this.goToLevel9, this,3,2,3,2);
        else
            this.level9Button = this.add.button(50, 448, 'Level9Button', this.goToLevel9, this,1,0,1,0);
        this.level9Button.anchor.setTo(0.0,0.0);

        if (this.game.levelManager.arrayOfHighScores[9] != this.game.levelManager.arrayOfMaxScores[9])
            this.level10Button = this.add.button(307, 448, 'Level10Button', this.goToLevel10, this,3,2,3,2);
        else
            this.level10Button = this.add.button(307, 448, 'Level10Button', this.goToLevel10, this,1,0,1,0);
        this.level10Button.anchor.setTo(0.0,0.0);

        if (this.game.levelManager.arrayOfHighScores[10] != this.game.levelManager.arrayOfMaxScores[10])
            this.level11Button = this.add.button(562, 448, 'Level11Button', this.goToLevel11, this,3,2,3,2);
        else
            this.level11Button = this.add.button(562, 448, 'Level11Button', this.goToLevel11, this,1,0,1,0);
        this.level11Button.anchor.setTo(0.0,0.0);

        if (this.game.levelManager.arrayOfHighScores[11] != this.game.levelManager.arrayOfMaxScores[11])
            this.level12Button = this.add.button(818, 448, 'Level12Button', this.goToLevel12, this,3,2,3,2);
        else
            this.level12Button = this.add.button(818, 448, 'Level12Button', this.goToLevel12, this,1,0,1,0);
        this.level12Button.anchor.setTo(0.0,0.0);

        this.returnToMenuButton = this.add.button(this.game.world.centerX-130, 700, 'MainMenuButton', this.goToMainMenu, this,1,0,1,0);
        this.returnToMenuButton.anchor.setTo(0.5,0.5);

        this.goToHighScoreButton = this.add.button(this.game.world.centerX+130, 700, 'HighScoreButton', this.goToHighScore, this,1,0,1,0);
        this.goToHighScoreButton.anchor.setTo(0.5,0.5);
    },

    update: function(){

    },

    /**
     * Function used to move to the game state cleanly
     * @param pointer object which clicked on the button which called this function
     */
    startGame: function(pointer){
        // Stop the intro music
        this.music.stop();
        // Move to game state
        this.state.start('Game');
    },

    /**
     * Function to set the current level to 1 before calling startGame()
     * @param pointer Pointer object which clicked on the button which called this function
     */
    goToLevel1: function(pointer){
        // Change current level to 1
        this.state.game.levelManager.currentLevelIndex = 0;
        this.startGame();
    },

    /**
     * Function to set the current level to 2 before calling startGame()
     * @param pointer Pointer object which clicked on the button which called this function
     */
    goToLevel2: function(pointer){

        // Change current level to 2
        this.state.game.levelManager.currentLevelIndex = 1;
        this.startGame();
    },

    /**
     * Function to set the current level to 3 before calling startGame()
     * @param pointer Pointer object which clicked on the button which called this function
     */
    goToLevel3: function(pointer){
        // Change current level to 3
        this.state.game.levelManager.currentLevelIndex = 2;
        this.startGame();
    },

    /**
     * Function to set the current level to 4 before calling startGame()
     * @param pointer Pointer object which clicked on the button which called this function
     */
    goToLevel4: function(pointer){
        // Change current level to 4
        this.state.game.levelManager.currentLevelIndex = 3;
        this.startGame();
    },

    /**
     * Function to set the current level to 5 before calling startGame()
     * @param pointer Pointer object which clicked on the button which called this function
     */
    goToLevel5: function(pointer){
        // Change current level to 5
        this.state.game.levelManager.currentLevelIndex = 4;
        this.startGame();
    },

    /**
     * Function to set the current level to 6 before calling startGame()
     * @param pointer Pointer object which clicked on the button which called this function
     */
    goToLevel6: function(pointer){
        // Change current level to 6
        this.state.game.levelManager.currentLevelIndex = 5;
        this.startGame();
    },

    /**
     * Function to set the current level to 7 before calling startGame()
     * @param pointer Pointer object which clicked on the button which called this function
     */
    goToLevel7: function(pointer){
        // Change current level to 7
        this.state.game.levelManager.currentLevelIndex = 6;
        this.startGame();
    },

    /**
     * Function to set the current level to 8 before calling startGame()
     * @param pointer Pointer object which clicked on the button which called this function
     */
    goToLevel8: function(pointer){
        // Change current level to 8
        this.state.game.levelManager.currentLevelIndex = 7;
        this.startGame();
     },

    /**
     * Function to set the current level to 9 before calling startGame()
     * @param pointer Pointer object which clicked on the button which called this function
     */
    goToLevel9: function(pointer){
        // Change current level to 9
        this.state.game.levelManager.currentLevelIndex = 8;
        this.startGame();
    },

    /**
     * Function to set the current level to 10 before calling startGame()
     * @param pointer Pointer object which clicked on the button which called this function
     */
    goToLevel10: function(pointer){
        // Change current level to 10
        this.state.game.levelManager.currentLevelIndex = 9;
        this.startGame();
     },

    /**
     * Function to set the current level to 11 before calling startGame()
     * @param pointer Pointer object which clicked on the button which called this function
     */
    goToLevel11: function(pointer){
        // Change current level to 11
        this.state.game.levelManager.currentLevelIndex = 10;
        this.startGame();
    },

    /**
     * Function to set the current level to 12 before calling startGame()
     * @param pointer Pointer object which clicked on the button which called this function
     */
    goToLevel12: function(pointer){
        // Change current level to 12
        this.state.game.levelManager.currentLevelIndex = 11;
        this.startGame();
    },

    /**
     * Function to set go to the main menu state cleanly
     * @param pointer Pointer object which clicked on the button which called this function
     */
    goToMainMenu: function(pointer){
        // Stop music track
        this.music.stop();
        // Go to Main menu
        this.state.start('MainMenu');
    },

    /**
     * Function to set go to the Highscore state cleanly
     * @param pointer Pointer object which clicked on the button which called this function
     */
    goToHighScore: function(pointer){
        // Stop music track
        this.music.stop();
        // Go to Main menu
        this.state.start('HighScore');
    }
};
