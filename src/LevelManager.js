/**
 * Created with Webstorm
 * User: B00216877
 */
 function LevelManager(){

    /**
     * Loads level 1
     * @param {state} the current game state object
     */
    this.loadLevel1 = function(state){

        // Setup planet positions and enable collision with spaceship
        state.planets = state.game.add.group();

        var sprite = state.planets.create(5000, 5000, 'planet3');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(48);
        sprite.body.setCollisionGroup(state.planetCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);

        // Setup collectable positions and enable collision with spaceship
        state.collectables = state.game.add.group();

        sprite = state.collectables.create(state.game.world.centerX, state.game.world.centerY-200, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

}
    /**
     * Loads level 2
     * @param {state} the current game state object
     */
     this.loadLevel2 = function(state) {
         // Setup planet positions and enable collision with spaceship
         state.planets = state.game.add.group();

         var sprite = state.planets.create(5000,5000, 'planet3');
         state.physics.p2.enable(sprite, false);
         sprite.body.static = true;
         sprite.body.setCircle(48);
         sprite.body.setCollisionGroup(state.planetCollisionGroup);
         sprite.body.collides(state.spaceShipCollisionGroup);

         // Setup star positions and enable collision with spaceship
         state.collectables = state.game.add.group();

         sprite = state.collectables.create(state.game.world.centerX-110, state.game.world.centerY-110, 'alienArtifact');
         state.physics.p2.enable(sprite, false);
         sprite.body.static = true;
         sprite.body.setCircle(18);
         sprite.body.data.shapes[0].sensor = true;
         sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
         sprite.body.collides(state.spaceShipCollisionGroup);
         sprite.animations.add("idle",null, 15, true, true);
         sprite.animations.play("idle", 15, true, false);

         sprite = state.collectables.create(state.game.world.centerX-220, state.game.world.centerY-220, 'alienArtifact');
         state.physics.p2.enable(sprite, false);
         sprite.body.static = true;
         sprite.body.setCircle(18);
         sprite.body.data.shapes[0].sensor = true;
         sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
         sprite.body.collides(state.spaceShipCollisionGroup);
         sprite.animations.add("idle",null, 15, true, true);
         sprite.animations.play("idle", 15, true, false);

         sprite = state.collectables.create(state.game.world.centerX-330, state.game.world.centerY-330, 'alienArtifact');
         state.physics.p2.enable(sprite, false);
         sprite.body.static = true;
         sprite.body.setCircle(18);
         sprite.body.data.shapes[0].sensor = true;
         sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
         sprite.body.collides(state.spaceShipCollisionGroup);
         sprite.animations.add("idle",null, 15, true, true);
         sprite.animations.play("idle", 15, true, false);
         }

    /**
     * Loads level 3
     * @param {state} the current game state object
     */
    this.loadLevel3 = function(state) {

        // Setup planet positions and enable collision with spaceship
        state.planets = state.game.add.group();

        var sprite = state.planets.create(5000,5000, 'planet3');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(48);
        sprite.body.setCollisionGroup(state.planetCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);

        // Setup star positions and enable collision with spaceship
        state.collectables = state.game.add.group();

        sprite = state.collectables.create(290, 170, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

        sprite = state.collectables.create(180, 60, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

        sprite = state.collectables.create(150, 210, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

    }

    /**
     * Loads level 4
     * @param {state} the current game state object
     */
    this.loadLevel4 = function(state) {
        // Setup planet positions and enable collision with spaceship
        state.planets = state.game.add.group();

        var sprite = state.planets.create(712,384, 'planet3');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(48);
        sprite.body.setCollisionGroup(state.planetCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);

        // Setup star positions and enable collision with spaceship
        state.collectables = state.game.add.group();

        sprite = state.collectables.create(830, 380, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

    }

    /**
     * Loads level 5
     * @param {state} the current game state object
     */
    this.loadLevel5 = function(state) {
        // Setup planet positions and enable collision with spaceship
        state.planets = state.game.add.group();

        var sprite = state.planets.create(312,384, 'planet3');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(48);
        sprite.body.setCollisionGroup(state.planetCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);

        // Setup star positions and enable collision with spaceship
        state.collectables = state.game.add.group();

        sprite = state.collectables.create(200, 380, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

        sprite = state.collectables.create(312, 484, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

        sprite = state.collectables.create(312, 284, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

    }

    /**
     * Loads level 6
     * @param {state} the current game state object
     */
    this.loadLevel6 = function(state) {
        // Setup planet positions and enable collision with spaceship
        state.planets = state.game.add.group();

        var sprite = state.planets.create(712,584, 'planet3');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(48);
        sprite.body.setCollisionGroup(state.planetCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);

        // Setup star positions and enable collision with spaceship
        state.collectables = state.game.add.group();

        sprite = state.collectables.create(640, 610, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

        sprite = state.collectables.create(780, 740, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

        sprite = state.collectables.create(880, 530, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

    }

    /**
     * Loads level 7
     * @param {state} the current game state object
     */
    this.loadLevel7 = function(state) {
        // Setup planet positions and enable collision with spaceship
        state.planets = state.game.add.group();

        var sprite = state.planets.create(306,384, 'planet3');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(48);
        sprite.body.setCollisionGroup(state.planetCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);


        sprite = state.planets.create(717, 384, 'planet4');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(48);
        sprite.body.setCollisionGroup(state.planetCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);

        // Setup star positions and enable collision with spaceship
        state.collectables = state.game.add.group();

        sprite = state.collectables.create(206, 384, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

        sprite = state.collectables.create(817, 384, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

    }

    /**
     * Loads level 8
     * @param {state} the current game state object
     */
    this.loadLevel8 = function(state) {
        // Setup planet positions and enable collision with spaceship
        state.planets = state.game.add.group();

        var sprite = state.planets.create(612,384, 'planet3');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(48);
        sprite.body.setCollisionGroup(state.planetCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);

        sprite = state.planets.create(812, 184, 'planet4');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(48);
        sprite.body.setCollisionGroup(state.planetCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);

        // Setup star positions and enable collision with spaceship
        state.collectables = state.game.add.group();

        sprite = state.collectables.create(715, 290, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

        sprite = state.collectables.create(900, 220, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

        sprite = state.collectables.create(700, 90, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);


    }

    /**
     * Loads level 9
     * @param {state} the current game state object
     */
    this.loadLevel9 = function(state) {
        // Setup planet positions and enable collision with spaceship
        state.planets = state.game.add.group();

        var sprite = state.planets.create(712,184, 'planet3');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(48);
        sprite.body.setCollisionGroup(state.planetCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);

        sprite = state.planets.create(412, 434, 'planet4');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(48);
        sprite.body.setCollisionGroup(state.planetCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);

        sprite =  state.planets.create(712, 534, 'planet7');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(48);
        sprite.body.setCollisionGroup(state.planetCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);

        // Setup star positions and enable collision with spaceship
        state.collectables = state.game.add.group();

        sprite = state.collectables.create(667, 95, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

        sprite = state.collectables.create(778, 581, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

        sprite = state.collectables.create(572, 521, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

    }

    /**
     * Loads level 10
     * @param {state} the current game state object
     */
    this.loadLevel10 = function(state) {

        // Setup planet positions and enable collision with spaceship
        state.planets = state.game.add.group();

        var sprite = state.planets.create(470,575, 'planet3');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(48);
        sprite.body.setCollisionGroup(state.planetCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);

        sprite = state.planets.create(300, 285, 'planet4');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(48);
        sprite.body.setCollisionGroup(state.planetCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);

        sprite =  state.planets.create(740, 350, 'planet7');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(48);
        sprite.body.setCollisionGroup(state.planetCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);

         // setup moons and enable collision with spaceship
         state.moons = state.game.add.group();

         var sprite = state.moons.create(470, 575, 'moon');
         state.physics.p2.enable(sprite, false);
         sprite.body.setCircle(13);
         //sprite.body.mass = Number.MAX_VALUE;
         sprite.body.setCollisionGroup(state.moonCollisionGroup);
         sprite.body.collides(state.spaceShipCollisionGroup);
         var constraint = state.game.physics.p2.createRevoluteConstraint(sprite.body, [ -50, -50 ], state.planets.getAt(0).body, [ 0, 0 ]);
         sprite.body.moveLeft(100);

        // Setup star positions and enable collision with spaceship
        state.collectables = state.game.add.group();

        sprite = state.collectables.create(370, 199, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

        sprite = state.collectables.create(660, 367, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

        sprite = state.collectables.create(572, 521, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);


    }

    /**
     * Loads level 11
     * @param {state} the current game state object
     */
    this.loadLevel11 = function(state) {

        // Setup planet positions and enable collision with spaceship
        state.planets = state.game.add.group();

        var sprite = state.planets.create(310,530, 'planet3');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(48);
        sprite.body.setCollisionGroup(state.planetCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);

        sprite = state.planets.create(732, 192, 'planet4');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(48);
        sprite.body.setCollisionGroup(state.planetCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);

        var sprite = state.planets.create(200,100, 'planet7');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(48);
        sprite.body.setCollisionGroup(state.planetCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);

        // setup moons and enable collision with spaceship
        state.moons = state.game.add.group();

        var sprite = state.moons.create(310, 530, 'moon');
        state.physics.p2.enable(sprite, false);
        sprite.body.setCircle(13);
        sprite.body.setCollisionGroup(state.moonCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        var constraint = state.game.physics.p2.createRevoluteConstraint(sprite.body, [ -50, -50 ], state.planets.getAt(0).body, [ 0, 0 ]);
        sprite.body.moveLeft(500);

        var sprite = state.moons.create(732, 192, 'moon');
        state.physics.p2.enable(sprite, false);
        sprite.body.setCircle(13);
        sprite.body.setCollisionGroup(state.moonCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        var constraint = state.game.physics.p2.createRevoluteConstraint(sprite.body, [ -80, -80 ], state.planets.getAt(1).body, [ 0, 0 ]);
        sprite.body.moveLeft(100);

        var sprite = state.moons.create(732, 192, 'Asteroids');
        state.physics.p2.enable(sprite, false);
        sprite.body.setCircle(93);
        sprite.body.setCollisionGroup(state.moonCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        var constraint = state.game.physics.p2.createRevoluteConstraint(sprite.body, [ 0, 0 ], state.planets.getAt(1).body, [ 0, 0 ]);
        sprite.body.rotateRight(5);

        // Setup star positions and enable collision with spaceship
        state.collectables = state.game.add.group();

        sprite = state.collectables.create(180, 630, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

        sprite = state.collectables.create(875, 140, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

        sprite = state.collectables.create(840, 290, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

    }

    /**
     * Loads level 12
     * @param {state} the current game state object
     */
    this.loadLevel12 = function(state) {
        // Setup planet positions and enable collision with spaceship
        state.planets = state.game.add.group();

        var sprite = state.planets.create(280,280, 'planet3');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(48);
        sprite.body.setCollisionGroup(state.planetCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);

        sprite = state.planets.create(750, 545, 'planet4');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(48);
        sprite.body.setCollisionGroup(state.planetCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);

        // setup moons and enable collision with spaceship
        state.moons = state.game.add.group();

        var sprite = state.moons.create(750, 545, 'moon');
        state.physics.p2.enable(sprite, false);
        sprite.body.setCircle(13);
        sprite.body.setCollisionGroup(state.moonCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        var constraint = state.game.physics.p2.createRevoluteConstraint(sprite.body, [ -80, -80 ], state.planets.getAt(1).body, [ 0, 0 ]);
        sprite.body.moveLeft(500);

        var sprite = state.moons.create(280, 280, 'moon');
        state.physics.p2.enable(sprite, false);
        sprite.body.setCircle(13);
        sprite.body.setCollisionGroup(state.moonCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        var constraint = state.game.physics.p2.createRevoluteConstraint(sprite.body, [ -90, -90 ], state.planets.getAt(0).body, [ 0, 0 ]);
        sprite.body.moveLeft(500);

        var sprite = state.moons.create(280, 280, 'Asteroids');
        state.physics.p2.enable(sprite, false);
        sprite.body.setCircle(93);
        sprite.body.setCollisionGroup(state.moonCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        var constraint = state.game.physics.p2.createRevoluteConstraint(sprite.body, [0,0], state.planets.getAt(0).body, [ 0, 0 ]);
        sprite.body.rotateLeft(5);

        var sprite = state.moons.create(770, 601, 'Asteroids');
        state.physics.p2.enable(sprite, false);
        sprite.body.setCircle(93);
        sprite.body.setCollisionGroup(state.moonCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        var constraint = state.game.physics.p2.createRevoluteConstraint(sprite.body, [0, 0], state.planets.getAt(1).body, [ 0, 0 ]);
        sprite.body.rotateRight(10);

        // Setup star positions and enable collision with spaceship
        state.collectables = state.game.add.group();

        sprite = state.collectables.create(900, 630, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

        sprite = state.collectables.create(124, 100, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);

        sprite = state.collectables.create(840, 290, 'alienArtifact');
        state.physics.p2.enable(sprite, false);
        sprite.body.static = true;
        sprite.body.setCircle(18);
        sprite.body.data.shapes[0].sensor = true;
        sprite.body.setCollisionGroup(state.collectablesCollisionGroup);
        sprite.body.collides(state.spaceShipCollisionGroup);
        sprite.animations.add("idle",null, 15, true, true);
        sprite.animations.play("idle", 15, true, false);


    }

    /**
     * Clears the current high scores
     */
    this.clearHighScores = function()
    {
        this.arrayOfHighScores = [0,0,0,0,0,0,0,0,0,0,0,0];
    }

    /** Array of all the load level functions the index can be used to load the appropriate level */
    this.arrayOfFunctions = [this.loadLevel1,this.loadLevel2,this.loadLevel3, this.loadLevel4, this.loadLevel5, this.loadLevel6, this.loadLevel7, this.loadLevel8, this.loadLevel9, this.loadLevel10, this.loadLevel11, this.loadLevel12];
    /** Array of all the current high scores for each level*/
    this.arrayOfHighScores = [0,0,0,0,0,0,0,0,0,0,0,0];
    /** Array of all the possible high scores for each level*/
    this.arrayOfMaxScores = [13, 33, 33, 13, 33, 33, 23, 33, 33, 33, 33, 33];
    /** Holds the players current level*/
    this.currentLevelIndex = 0;
}
