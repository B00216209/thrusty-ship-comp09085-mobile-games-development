/**
 * Created with Webstorm using the template from https://github.com/photonstorm/phaser/tree/master/resources/Project%20Templates/Full%20Screen%20Mobile
 * User: B00216209
 */
BasicGame.Game = function(game){
	//	When a State is added to Phaser it automatically has the following properties set on it, even if they already exist:
    this.game;		//	a reference to the currently running game
    this.add;		//	used to add sprites, text, groups, etc
    this.camera;	//	a reference to the game camera
    this.cache;		//	the game cache
    this.input;		//	the global input manager (you can access this.input.keyboard, this.input.mouse, as well from it)
    this.load;		//	for preloading assets
    this.math;		//	lots of useful common math operations
    this.sound;		//	the sound manager - add a sound, play one, set-up markers, etc
    this.stage;		//	the game stage
    this.time;		//	the clock
    this.tweens;	//	the tween manager
    this.world;		//	the game world
    this.particles;	//	the particle manager
    this.physics;	//	the physics manager
    this.rnd;		//	the repeatable random number generator
    this.spaceship; // the spaceship sprite
    this.spaceShipCollisionGroup;       // the spaceship's collision group
    this.collectables;  // the group of collectable sprites
    this.collectablesCollisionGroup;    // the collectable's collision group
    this.planets;   // the group of planet sprites
    this.planetCollisionGroup;  // the planet's collision group
    this.moons;     // the group of moon sprites
    this.moonCollisionGroup;    // the moon's collision group
    this.catchFlag; // boolean value for signifying the spaceship being under pointer control
    this.tracerArrow;   // tracer arrow sprite used for indicating the vector from pointer to spaceship
    this.hasLaunched;   // boolean value signifying the spaceship being launched(and under gravity effects) or not
    this.currentScore;  // number for current score
    this.highScore; // number for high score
    this.ARCADE;    // Physics.ARCADE object used for setting sprite rotations
    this.pickupSound;   // audio object for pickup sound
    this.backgroundAudio;   // audio object for background audio
    this.lives;     // number of current lives used for drawing life sprites
    this.livesText; // sprite which displays "Lives: " before the life sprites
    this.restartButton; // restart button
    this.deathScreen;   // boolean value for signifying the state of death, being in death state prevents any updates to the spaceship from taking place
    this.explosionSound;    // sound object for the explosion sound
    this.levelSelectButton; // level select button
    this.lives1;    // 1 life remaining sprite
    this.lives2;    // 2 lives remaining sprite
    this.lives3;    // 3 lives remaining sprite
    this.gravity;   // Gravity value used for calculating planet pull on the spaceship
    this.summedGravityVector;   // Summed gravity vector is calculated each update and added to the spaceships velocity
    this.shipVec;   // vector from the origin to the spaceship
    this.tempPlanet;    // temp planet object used during gravity calculations
    this.planetVec; // vector from the origin to the temp planet used during gravity calculations
    this.shipToPlanetVec;   // vector from the ship to the temp planet used to attenuate gravity during the gravity calculations
    this.gravityPull;   // magnitude of gravity for the current temp planet used during gravity calculations
    this.boostUsed; // boolean value signifying the use of the boost ability
};

BasicGame.Game.prototype = {

    /**
     * Function used as a callback on pointer down event to set the catchflag boolean to true
     */
    set: function(){
        // if not in deathscreen set catchFlag to true
        if(this.deathScreen == false) {
            this.catchFlag = true;
        }
    },

    /**
     * Function used as a callback on pointer up event, used for launching the ship or applying a boost
     */
    launch: function(){
        // if not in death screen...
        if(this.deathScreen == false){
            // if ship has not already launched
            if (this.hasLaunched == false){
                // set catch flag and has launched booleans to appropriate values
                this.catchFlag = false;
                this.hasLaunched = true;
                // get x and Y vectors from tracerArrow
                var Xvector = ( - this.tracerArrow.elements[0] * 1.0);
                var Yvector = ( - this.tracerArrow.elements[1] * 1.0);
                // set spaceship velocity to x and y vectors obtained from tracer arrow, with Y flipped sign due to window coordinates
                this.spaceship.body.moveRight(Xvector);
                this.spaceship.body.moveUp(-Yvector);
            }
            else{
                // if boost is not used...
                if(this.boostUsed == false){
                    // set catchFlag to false
                    this.catchFlag = false;
                    // update tracerArrow with the current vector from the mouse to the ship
                    this.tracerArrow.elements[0] = (this.spaceship.body.x - this.game.input.x);
                    this.tracerArrow.elements[1] = (this.spaceship.body.y - this.game.input.y);
                    // create and X and Y vectors from the tracer arrow elements
                    var Xvector = ( this.tracerArrow.elements[0] * 1.0);
                    var Yvector = ( this.tracerArrow.elements[1] * 1.0);
                    // add the x and Y vectors to the spaceship's current velocity (note conversion from internal P2 velocity back to pixels/s)
                    this.spaceship.body.moveRight((this.spaceship.body.velocity.x * -20) + Xvector);
                    this.spaceship.body.moveUp((this.spaceship.body.velocity.y * 20) + (-Yvector));
                    // set boost used to true
                    this.boostUsed = true;
                }
            }
        }
    },

	create: function(){
        // initialise variables
        this.currentScore = 0;
        this.highScore = 0;
        this.tracerArrow = Vector.Zero(2);
        this.catchFlag = false;
        this.hasLaunched = false;
        this.lives = 3;
        this.deathScreen = false;
        this.gravity = 50000;
        this.summedGravityVector = Vector.Zero(2);
        this.boostUsed = false;

        // Setup audio and start background track
        this.pickupSound = this.add.audio('pickup');
        this.explosionSound = this.add.audio('explosion');
        this.backgroundAudio = this.add.audio('level1',1,true);
        this.backgroundAudio.play();

        // Setup game background image
        this.game.add.sprite(0, 0,'spacescape');

        // Setup physics system
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.physics.startSystem(Phaser.Physics.P2JS);
        this.game.physics.p2.setImpactEvents(true);
        this.game.physics.p2.defaultRestitution = 0.8;
        this.game.physics.p2.defaultFriction = 0;
        this.game.physics.p2.applyDamping = false;
        this.game.physics.p2.applyGravity = false;
        this.game.physics.p2.applySpringForces = false;
        this.game.physics.p2.frameRate = (1/60);
        this.spaceShipCollisionGroup = this.game.physics.p2.createCollisionGroup();
        this.collectablesCollisionGroup = this.game.physics.p2.createCollisionGroup();
        this.planetCollisionGroup = this.game.physics.p2.createCollisionGroup();
        this.moonCollisionGroup = this.game.physics.p2.createCollisionGroup();
        this.game.physics.p2.updateBoundsCollisionGroup();

        // Setup spaceship
        this.spaceship = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'destroyer');
        this.game.physics.p2.enable(this.spaceship, false);
        this.spaceship.anchor.setTo(0.5, 0.5);
        this.spaceship.body.setCircle(25);
        this.spaceship.body.mass = 0.00001;
        this.spaceship.body.setCollisionGroup(this.spaceShipCollisionGroup);
        this.spaceship.body.static = false;

        // Setup explosion animation
        this.explosions = this.game.add.sprite(0,0, 'explosion');
        this.explosions.animations.add('explosion');
        this.explosions.anchor.setTo(0.5,0.5);

        // Setup Lives text sprite and spaceship icons
        this.livesText = this.game.add.sprite(this.game.world.centerX + 300, 30, 'LivesText');
        this.livesText.anchor.setTo(0.5,0.5);
        this.lives1 = this.game.add.sprite(this.game.world.centerX + 375, 30, 'destroyer');
        this.lives1.anchor.setTo(0.5,0.5);
        this.lives2 = this.game.add.sprite(this.game.world.centerX + 425, 30, 'destroyer');
        this.lives2.anchor.setTo(0.5,0.5);
        this.lives3 = this.game.add.sprite(this.game.world.centerX + 475, 30, 'destroyer');
        this.lives3.anchor.setTo(0.5,0.5);

        // Load current level - sets up collectables and planets
        this.game.levelManager.arrayOfFunctions[this.game.levelManager.currentLevelIndex](this);

        // Set event handles for pointer down and pointer up calls
        this.game.input.onDown.add(this.set, this);
        this.game.input.onUp.add(this.launch, this);

        // Setup Level select and restart buttons
        this.levelSelectButton = this.game.add.button(this.game.world.centerX-115, 725, 'LevelSelectButton', this.quitGame, this, 1, 0, 1, 0);
        this.levelSelectButton.anchor.setTo(0.5, 0.5);
        this.restartButton = this.add.button(this.world.centerX+115, 725, 'RestartButton', this.restartLevel, this, 1,0,1,0);
        this.restartButton.anchor.setTo(0.5,0.5);

        // Setup callback function to call when spaceship collides with another body
        this.spaceship.body.collides(this.collectablesCollisionGroup, this.pickupCollectable, this );
        this.spaceship.body.collides(this.planetCollisionGroup, this.crash, this );
        this.spaceship.body.collides(this.moonCollisionGroup, this.crash, this );
        this.spaceship.body.onBeginContact.add(this.collisionResolver, this);
    },

	update: function(){
        // If not in death screen state (in which we want no updates to be done)
        if(this.deathScreen == false){
            // If ship has launched (and needs to be affected by gravity)
            if (this.hasLaunched == true){
                // Reset summed gravity vector to 0
                this.summedGravityVector = Vector.Zero(2);
                // Get the ships location in the world
                this.shipVec = $V([this.spaceship.body.x, this.spaceship.body.y]);
                // For each planet in the group calculate it's contribution to the summed gravity vector
                for (var i = this.planets.children.length; i > 0; i--){
                    // Get planet sprite
                    this.tempPlanet = this.planets.getAt(i - 1);
                    // Get the planet's location in the world
                    this.planetVec = $V([this.tempPlanet.body.x, this.tempPlanet.body.y]);
                    // Get the vector from the ship to the planet
                    this.shipToPlanetVec = this.planetVec.subtract(this.shipVec);
                    // Calculate the gravity pull magnitude for that planet based on it's distance from the ship using a 50/50 mix of linear distance and distance squared
                    this.gravityPull = this.gravity / (this.shipVec.distanceFrom(this.planetVec) * 0.5 + ((this.shipVec.distanceFrom(this.planetVec) * this.shipVec.distanceFrom(this.planetVec) * 0.5)));
                    // Calculate the gravity pull vector using the vector from the ship to the planet and the gravity pull magnitude
                    this.shipToPlanetVec = this.shipToPlanetVec.multiply((1 / (this.shipToPlanetVec.distanceFrom(Vector.Zero(2)))) * this.gravityPull);
                    // Add this gravity pull vector to the summedGravity vector
                    this.summedGravityVector = this.summedGravityVector.add(this.shipToPlanetVec);
                }
                // Add the summed gravity vector the the spaceships current velocity
                this.spaceship.body.moveRight((this.spaceship.body.velocity.x * -20) + (this.summedGravityVector.elements[0]));
                this.spaceship.body.moveUp((this.spaceship.body.velocity.y * 20) + (-this.summedGravityVector.elements[1]));
                // Update the spaceships rotation to match it's new velocity
                this.spaceship.body.rotation = this.game.physics.arcade.angleToXY(this.spaceship.body.velocity, 0, 0);
            } else {
                //  If the ship is not launched, update the tracerArrow Vector to be from the ship to last known pointer location
                this.tracerArrow.elements[0] = this.game.input.x - this.spaceship.body.x;
                this.tracerArrow.elements[1] = this.game.input.y - this.spaceship.body.y;
                // Update spaceship rotation to face away from the pointer
                this.spaceship.body.rotation = this.game.physics.arcade.angleBetween(this.game.input, this.spaceship);
            }
        }
    },

    render: function(){
    },

	quitGame: function(){
        // Stop game music
        this.backgroundAudio.stop();
        // Go back to level select
		this.state.start('LevelSelect');
	},

    /**
     * Function used when a spaceship collides with a pickup
     * @param spaceship spaceship body sent via onCollide funtion
     * @param pickup pickup body sent via onCollide function,
     */
    pickupCollectable: function(spaceship, pickup){
        // Play pickup collection Sound
        this.pickupSound.play();
        // Add 10 to score
        this.currentScore = this.currentScore + 10;
        // Kill the pickup that triggered this event
        pickup.sprite.kill();
        // If all collectables are dead either move onto next level or quit game if you just finished the last level
        if(this.collectables.countLiving() == 0){
            // Update current score with your remaining lives
            this.currentScore = this.currentScore + this.lives;
            // If current score is greater than the highscore saved for this level update the highscore.
            if(this.currentScore > this.game.levelManager.arrayOfHighScores[this.game.levelManager.currentLevelIndex]) this.game.levelManager.arrayOfHighScores[this.game.levelManager.currentLevelIndex] = this.currentScore;
            // If there is another level to play...
            if(this.game.levelManager.currentLevelIndex < this.game.levelManager.arrayOfFunctions.length - 1){
                // Set the current level index to the next level
                this.game.levelManager.currentLevelIndex++;
                // Stop the background audio
                this.backgroundAudio.stop();
                // Restart the game state(which will then load the new level index)
                this.state.restart('game');
            }
            // Else you have just finished the last level...
            else {
                // So quit the game to level select.
                this.quitGame();
            }
        }
    },

    /**
     * Function used when a spaceship collides with a planet or moon
     * @param spaceship spaceship body sent via onCollide funtion
     * @param planet planet body sent via onCollide function,
     */
    crash: function(spaceship, planet){
        // Move the explosion sprite to the ships current location
        this.explosions.reset(this.spaceship.body.x, this.spaceship.body.y);
        // Play the explosion animation
        this.explosions.play('explosion', 20, false, true);
        // Play the explosion sound
        this.explosionSound.play();
        // Reset ship back to the world center
        this.spaceship.reset(this.game.world.centerX,this.game.world.centerY);
         // If you have any lives remaining...
        this.boostUsed = false;
        if(this.lives > 0){
            // Decrement live count
            this.lives--;
            // Set spaceship velocity to 0
            this.spaceship.body.moveRight(0);
            this.spaceship.body.moveUp(0);
            // Set haslaunched back to false (to prevent gravity)
            this.hasLaunched = false;
            // kill whatever life sprite is appropriate
            switch(this.lives)
            {
                case 2: this.lives3.kill(); break;
                case 1: this.lives2.kill(); break;
                case 0: this.lives1.kill(); break;
            }
        }
        // else you have no lives and suffer a true death.
        else{
            // Set deathscreen to true which prevents both gravity and launch functionality
            this.deathScreen = true;
            // Kill spaceship sprite
            this.spaceship.kill();
        }
    },

    /**
     *  Function called to reset variables, sprites, lives and scores to their appropriate beginning state.
     */
    restartLevel: function(){
        // If you current score is greater than the highscore update the highscore
        if(this.currentScore > this.highScore) this.highScore = this.currentScore;
        // Reset variables, sprites, lives and scores to their appropriate beginning state.
        this.lives = 3;
        this.deathScreen = false;
        this.spaceship.revive();
        this.spaceship.reset(this.game.world.centerX,this.game.world.centerY);
        this.spaceship.body.moveRight(0);
        this.spaceship.body.moveUp(0);
        this.collectables.callAll('revive');
        this.lives1.revive();
        this.lives2.revive();
        this.lives3.revive();
        this.currentScore = 0;
        this.hasLaunched = false;
        this.boostUsed = false;
    },

    /**
     * Function used to resolve collisions between the spaceship and objects which are set to be sensors such as the collectable
     * @param body the body which the spaceship body collided with
     * @param shapeA the shape object belonging to the spaceship body
     * @param shapeB the shape object belonging to the colliding body
     * @param equation the collision equation used to resolve the collision
     */
    collisionResolver: function (body, shapeA, shapeB, equation){
        // If body is not null (collision with walls have a null body)
        if(body != null){
            // then confirm that the object you collided with was the collectable
            if (body.sprite.key == 'alienArtifact'){
                // Call appropriate function to record the pickup
                this.pickupCollectable(this.spaceship, body);
            }
        }
    }
};
