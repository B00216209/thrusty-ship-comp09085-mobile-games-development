/**
 * Created with Webstorm using the template from https://github.com/photonstorm/phaser/tree/master/resources/Project%20Templates/Full%20Screen%20Mobile
 * User: B00216209
 */
BasicGame.MainMenu = function(game){
    this.music;     // music object used for intro music
    this.newGameButton; // Button for starting a fresh game
    this.levelSelectButton;   // Button for going to level select state
    this.splashLogo;    // Sprite for the game logo
};

BasicGame.MainMenu.prototype = {

    create: function(){
        // Setup Background image
        this.game.add.sprite(0, 0,'spacescape');

        // Setup game logo and set it to tween between a 25 drop and a 25 raise.
        this.splashLogo = this.game.add.sprite(43, 84,'SplashLogo');
        this.game.add.tween(this.splashLogo).to({y: '+25'}, 1000, Phaser.Easing.Linear.none).to({y: '-25'}, 1000, Phaser.Easing.Linear.none).loop().start();

        // Setup intro music and start playing.
        this.music = this.add.audio('intro');
        this.music.play();

        // NewGame and highScore buttons
        this.newGameButton = this.add.button(this.world.centerX - 130, this.world.centerY+120, 'NewGameButton', this.startGame, this, 1,0,1,0);
        this.newGameButton.anchor.setTo(0.5,0.5);
        this.levelSelectButton = this.add.button(this.world.centerX+130, this.world.centerY+120, 'LevelSelectButton', this.levelSelect, this,1,0,1,0);
        this.levelSelectButton.anchor.setTo(0.5,0.5);
    },

    update: function(){
    },

    /**
     * Function used to start a fresh game
     * @param pointer The pointer object which cliked on the button triggering this callback function
     */
    startGame: function(pointer){
        //	Ok, the Play Button has been clicked or touched, so let's stop the music (otherwise it'll carry on playing)
        this.music.stop();
        // Set current level to 1 (index 0)
        this.game.levelManager.currentLevelIndex = 0;
        // Clear old highscores
        this.game.levelManager.clearHighScores();
        // Start the game
        this.state.start('Game');
    },

    /**
     * Function used to go to the level select state
     */
    levelSelect: function(){
        //	Ok, the Play Button has been clicked or touched, so let's stop the music (otherwise it'll carry on playing)
        this.music.stop();
        // Start level Select
        this.state.start('LevelSelect');
    }
};
