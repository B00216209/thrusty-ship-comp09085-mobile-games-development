/**
 * Created with Webstorm using the template from https://github.com/photonstorm/phaser/tree/master/resources/Project%20Templates/Full%20Screen%20Mobile
 * User: B00216209
 */
BasicGame.HighScore = function (game){
    this.music; // music object used to play intro music
    this.returnToLevelSelectButton;    // return to menu button
    this.highScoreSubmitButton; // Submit HighScore button
    this.highScoreTable; // Table displaying current Highscores
    this.highScoreTableHeader;  // Header text for the Highscore Table
    this.highScoreBackGround; // UI appropriate sprite for holding highscore table text
    this.currentHighScoreText; // Text displaying Current Highscore
};

BasicGame.HighScore.prototype = {

    create: function(){
        // Setup background image
        this.game.add.sprite(0, 0,'spacescape');

        // Setup intro music and start it playing
        this.music = this.add.audio('intro');
        this.music.play();

        // Setup highscore background sprite
        this.highScoreBackGround = this.add.sprite(this.game.world.centerX, this.game.world.centerY,'HighScoreBackground');
        this.highScoreBackGround.anchor.setTo(0.5,0.5);

        // Setup button to return to level select
        this.returnToLevelSelectButton = this.add.button(this.game.world.centerX-130, 700, 'LevelSelectButton', this.goToLevelSelect, this,1,0,1,0);
        this.returnToLevelSelectButton.anchor.setTo(0.5,0.5);

        // Setup button to submit highscore to the server
        this.highScoreSubmitButton = this.add.button(this.game.world.centerX+130, 700, 'SubmitButton', this.submitHighScore, this,1,0,1,0);
        this.highScoreSubmitButton.anchor.setTo(0.5,0.5);

        // Setup Highscore table header text
        this.highScoreTableHeader = this.add.bitmapText(50, 50, 'HighLightFont');
        this.highScoreTableHeader.size = 40;
        this.highScoreTableHeader.setText('TOP 10 HIGHSCORES');
        this.highScoreTableHeader.align = 'left';

        // Compute current highscore and setup current highscore text sprite
        var totalScore = 0;
        for(var i = this.game.levelManager.arrayOfHighScores.length; i != 0; i--){
            totalScore += this.game.levelManager.arrayOfHighScores[i-1];
        }

        this.currentHighScoreText = this.add.bitmapText(50, 613, 'HighLightFont');
        this.currentHighScoreText.size = 30;
        this.currentHighScoreText.setText('YOUR HIGHSCORE: '+totalScore);
        this.currentHighScoreText.align = 'left';

        // Setup highscore table sprite and update with the current table.
        this.highScoreTable = this.add.bitmapText(50, 140, 'HighLightFont');
        this.highScoreTable.size = 30;
        this.highScoreTable.setText('');
        this.highScoreTable.align = 'left';
        setInterval(this.getTable(),3000);
    },

    update: function(){
        // Re-center all text objects
        this.highScoreTableHeader.position.x = this.game.world.centerX - (this.highScoreTableHeader.textWidth / 2);
        this.highScoreTable.position.x = this.game.world.centerX - (this.highScoreTable.textWidth / 2);
        this.currentHighScoreText.position.x = this.game.world.centerX - (this.currentHighScoreText.textWidth / 2);

        // periodically check for highscore table updates

    },

    /**
     * Function used to move to the level select state cleanly
     * @param pointer Pointer object which clicked on the button which called this function
     */
    goToLevelSelect: function(pointer){
        // Stop the intro music
        this.music.stop();
        // Move to level select state
        this.state.start('LevelSelect');
    },

    /**
     * Function used to gather the data needed to submit a highscore then submits that highscore using submitScore()
     * @param pointer Pointer object which clicked on the button which called this function
     */
    submitHighScore: function(pointer){
        // Sum current highscores
        var totalScore = 0;
        for(var i = this.game.levelManager.arrayOfHighScores.length; i != 0; i--){
            totalScore += this.game.levelManager.arrayOfHighScores[i-1];
        }
        // Get current Date to use as name
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd<10) {
            dd='0'+dd
        }
        if(mm<10) {
            mm='0'+mm
        }
        today = dd+'/'+mm+'/'+yyyy;
        // Submit the highscore to the online scoreboard
        this.submitScore("ThrustyShip",today,"devlin1991@gmail.com",totalScore);
        this.currentHighScoreText.setText('YOUR HIGHSCORE: '+ totalScore);
    },

    /**
     * Function which interfaces with https://mcm-highscores-hrd.appspot.com/ to submit a high score
     * @param gameName  Name of the game
     * @param name      Username to be displayed on the highscore table
     * @param email     Email address registered to the game
     * @param score     Score to be submitted
     */
    submitScore: function(gameName,name,email,score) {
        var url = this.game.appURL + "score?game={0}&nickname={1}&email={2}&score={3}&func=?";
        url = url.replace('{0}', gameName);
        url = url.replace('{1}', name);
        url = url.replace('{2}', email);
        url = url.replace('{3}', score);
        var tempState = this;
        console.log(url);
        $.ajax({
            type: "GET",
            url: url,
            async: true,
            contentType: 'application/json',
            dataType: 'jsonp',
            success: function (json) {
                console.log(json.result);
            },
            error: function (e) {
                window.alert(e.message);
                tempState.highScoreTable.setText('Unable to Connect to HighScore Server!\n');
                tempState.highScoreTable.position.x = tempState.game.world.centerX - (tempState.highScoreTable.textWidth / 2);
            }
        });
    },

    /**
     * Function to update highScoreTable text using a json object
     * @param obj json object to parse to generate highscore table
     */
    showScoreTable: function(obj){
        var s = "", i;
        for (i = 0; i < obj.scores.length; i += 1) {
            s += obj.scores[i].name + ' : ' + obj.scores[i].score + " ";
            s+= '\n'
        }
        this.highScoreTable.setText(s);
        this.highScoreTable.position.x = this.game.world.centerX - (this.highScoreTable.textWidth / 2);
    },

    /**
     * Function which uses a ajax call to get the current highscore table from a server.
     */
    getTable: function(){
        var url = this.game.appURL + "scoresjsonp?game=ThrustyShip&func=?";
        var tempState = this;
        $.ajax({
            type: "GET",
            url: url,
            async: true,
            // Note, instead of this we could have a success function...
            //jsonpCallback: 'showScoreTable',
            contentType: 'application/json',
            dataType: 'jsonp',
            success: function (json) {
                tempState.showScoreTable(json);
            },
            error: function (e) {
                window.alert(e.message);
                tempState.highScoreTable.setText('Unable to Connect to HighScore Server!\n');
                tempState.highScoreTable.position.x = tempState.game.world.centerX - (tempState.highScoreTable.textWidth / 2);
            }
        });
    }
};
