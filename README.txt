This is a repo for hosting the Mobile Games Development Part 2 Assignment for B00216209 and B00216877.

-=Credits=-
Planet, moon and spaceship art are modified assets from http://millionthvector.blogspot.de,  provided under the Creative Commons BY License: http://creativecommons.org/licenses/by/3.0/

Pickup sound is from Yo Frankie! game, http://opengameart.org/content/life-pickup-yo-frankie,  provided under the Creative Commons BY License: http://creativecommons.org/licenses/by/3.0/

Space background image is a modified asset from http://opengameart.org/content/space-backgrounds-3, provided under the Creative Commons CC0 Licence: http://creativecommons.org/publicdomain/zero/1.0/

Background audio is from http://opengameart.org/content/stereotypical-90s-space-shooter-music, provided under the Creative Commons BY License: http://creativecommons.org/licenses/by/3.0/

UI Buttons are modified assets from http://opengameart.org/content/sci-fi-user-interface-elements, provided under Creative Commons CC0 Licence:
http://creativecommons.org/publicdomain/zero/1.0/

Explosion audio is from http://www.freesound.org/people/sarge4267/sounds/102733/, provided under the Creative Commons BY License:
http://creativecommons.org/licenses/by/3.0/

